

import 'package:flutter/material.dart';
import 'package:distrimaps1/main.dart';

class Description extends StatefulWidget {
  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
          centerTitle: true,
        title: new Text('DESCRIPTION')),


      body: SingleChildScrollView(
        // Center is a lyout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Container(
                      height: 800,
                      width: 356, margin: const EdgeInsets.only(top: 5, left: 4, right: 4),
                    child: Text("\nFR: Société de transporteurs, chauffeurs routiers et visiteurs,\ncette application vous fait gagner du temps quelque soit votre position de départ partout en Europe.Elle vous guidera jusqu'à nos entrepôts sans aucun risque de vous perdre."
                        "\n\nENG: Company of transportations, truck drivers and visitors,"
                        "\nthis application saves you time regardless of your starting position anywhere in Europe. It will guide you to our warehouses without any risk of getting lost.\n\nESP: Empresa de transportistas, camioneros y visitantes,\nesta aplicación le ahorra tiempo sea cual sea su posición de partida en cualquier lugar de Europa. Le guiará hasta nuestros almacenes sin riesgo de perderse."
                      "\n\nRU: Компания по перевозкам, водители грузовиков и посетители,\nэто приложение экономит ваше время, независимо от вашей стартовой позиции в любой точке Европы.Оно поможет вам добраться до наших складов без риска заблудиться.\n\nIT : Compagnia di trasportatori, camionisti e visitatori,\nquesta applicazione ti fa risparmiare tempo ovunque tu sia in Europa. Vi guiderà ai nostri magazzini senza alcun rischio di perdersi.",

                      style: TextStyle(
                        fontSize: 18,

                        color: Colors.indigo[900],
                      ),
                    ),
                  ))


                ],
              )

            ]),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
