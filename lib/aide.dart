import 'package:flutter/material.dart';
import 'package:distrimaps1/main.dart';

class Aide extends StatefulWidget {
  Aide({Key key}) : super(key: key);

  @override
  _AideState createState() => _AideState();
}

class _AideState extends State<Aide> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('HELP')),
        centerTitle: true,
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(

                    height: 250,
                    width: 240,
                    child: Text("*connaitre son lieu de chargement *Chaque batîment est representé par des couleurs pour vous retouver lorque vous étés au volant*cliquer sur votre lieu de chargement ou scanner le qrcode et suivre les instructions du gps , maps"),
                  )
                ],
              )
            ]),
      ),
    );
  }
}
