
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:distrimaps1/description.dart';
import 'package:distrimaps1/restaurant.dart';
import 'package:distrimaps1/russe.dart';
import 'package:share/share.dart';

class Russe extends StatefulWidget {
  Russe({Key key}) : super(key: key);

  @override
  _RusseState createState() => _RusseState();
}

class _RusseState extends State<Russe> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        bottomNavigationBar: Container(
          decoration: BoxDecoration(border: Border.all(width: 0)),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.restaurant_menu ),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/restaurant');
                    }),
                IconButton(
                    icon: Icon(Icons.question_answer_outlined),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/description');
                    }),
              ],
            ),
          ),
        ),
        appBar: AppBar(
          centerTitle: true,
            title: Text('Р У С С К И Й   Я З Ы К')),

          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                DrawerHeader(
                    child: Container(
                      height: 142,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.yellow,
                      child: Image.asset('images/camion1.png',
                        fit: BoxFit.fitHeight,

                      ),
                    )),

                GestureDetector(
                  onTap: () {
                    final RenderBox box = context.findRenderObject();
                    Share.share('https://play.google.com/store/apps/details?id=com.asibro.distrimaps1',
                        sharePositionOrigin:
                        box.localToGlobal(Offset.zero) &
                        box.size);
                  },
                  child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(top: 50, bottom: 100, left: 15, right: 15),
                      child: ListTile(
                        leading: Icon(Icons.share,
                          color: Colors.black,
                        ),
                        title: Text("Чтобы порекомендовать приложение distrimaps\nНажмите здесь",

                        ),
                      )

                  ),
                ),

                GestureDetector(

                  onTap:() async {
                    const url = 'mailto:as.salami2021@gmail.com';

                    if (await canLaunch(url)) {
                      await launch(url, forceSafariVC: false);
                    } else {
                      throw 'Could not launch $url';
                    }
                  },
                  child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(bottom: 100, left: 15, right: 15),
                      child: ListTile(
                        leading: Icon(Icons.contact_mail,
                          color: Colors.black,
                        ),
                        title: Text('Контактное устройство\nНажмите здесь',

                        ),
                      )

                  ),
                ),
                Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(bottom: 100, left: 15, right: 15),
                    child: ListTile(
                      leading: Icon(Icons.info_outline,
                        color: Colors.black,
                      ),
                      title: Text("Версия приложения\n1.0.3",

                      ),
                    )
                ),
              ],
            ),
          ),


        ),

        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child:  Container(
                      decoration: BoxDecoration(
                      ),

                      margin: const EdgeInsets.only(bottom: 0, top: 1, left: 10, right: 10),
                      child:
                      Image.asset(
                        'images/plan1.png',
                        fit: BoxFit.fill,
                      ),
                    ),

                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  Container(
                    width: 200,
                    child: Image.asset('images/corona.jpg',
                      fit: BoxFit.fill,
                    ),

                  ),


                  Expanded(
                      child: Container(
                          height: 160,
                          width: 197,

                          margin: const EdgeInsets.only(left: 4),
                          child: Center(
                            child: Text("Пожалуйста, предъявите CMR на приеме экспедиции.",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.indigo[900],
                              ),
                            ),
                          )
                      )
                  )

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//Distrimag,+Fos-sur-Mer/@43.4799073,4.8173402,12z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b619725c13d7bd:0x2644b07d96034e83!2m2!1d4.8873801!2d43.4799287!3e0?hl=fr';

                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.grey[400],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 130,
                            width: 200,
                            margin: const EdgeInsets.only(top: 15, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТЫ FOS LA FEUILLANE-ИНФОРМАЦИЯ НА СТЕНДЕ", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),)

                  ),

                  Container(
                    child: Image.asset('images/fossurmer.png'),
                    color: Colors.grey[400],
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 15, left: 4),
                  ),

                  Container(
                    color: Colors.grey[400],
                    height: 100,
                    width: 55,
                    margin: const EdgeInsets.only(top: 15, left: 6),
                  )

                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//Rue+Lavoisier,+13310+Saint-Martin-de-Crau/@43.6267414,4.798866,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x12b60be59239822f:0x824b4875fa4ae31d!2m2!1d4.8010547!2d43.6267375?hl=fr';

                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.purple[100],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 135,
                            width: 200,
                            margin: const EdgeInsets.only(top: 50, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТ J\nSAINT MARTIN DE CRAU / ОФИСНАЯ ЭКСПЕДИЦИЯ\nДВЕРЬ 14", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),
                      )

                  ),
                  Container(
                    child:
                    Image.asset('images/BATJ.png'),
                    color: Colors.purple[100],
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 50, left: 6),
                  ),
                  Container(
                    color: Colors.purple[100],
                    height: 100,
                    width: 55,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//Draille+des+Morts,+Saint-Martin-de-Crau/@43.6308653,4.7708582,15z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60b9781a2d0e3:0xdec61982f21d80a4!2m2!1d4.779613!2d43.6308656!3e0?hl=fr';

                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.lightBlue,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 135,
                            width: 200,
                            margin: const EdgeInsets.only(top: 50, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТ A\nSAINT MARTIN DE CRAU / ОФИСНАЯ ЭКСПЕДИЦИЯ\nДВЕРЬ 66", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),
                      )

                  ),
                  Container(
                    child: Image.asset('images/bat c.png'),
                    color: Colors.lightBlue,
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                  ),
                  Container(
                    color: Colors.lightBlue,
                    height: 100,
                    width: 55,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                    child: Center(
                      child: Text(
                        'После направления садового поста налево',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 13.5,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//Draille+des+Morts,+Saint-Martin-de-Crau/@43.6308653,4.7708582,15z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60b9781a2d0e3:0xdec61982f21d80a4!2m2!1d4.779613!2d43.6308656!3e0?hl=fr';
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.lightBlueAccent[100],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 135,
                            width: 200,
                            margin: const EdgeInsets.only(top: 50, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТ B1\nSAINT MARTIN DE CRAU / ОФИСНАЯ ЭКСПЕДИЦИЯ\nДВЕРЬ 29", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),
                      )

                  ),
                  Container(
                    child: Image.asset('images/bat c.png'),
                    color: Colors.lightBlueAccent[100],
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                  ),
                  Container(
                      color: Colors.lightBlueAccent[100],
                      height: 100,
                      width: 55,
                      margin: const EdgeInsets.only(top: 50, left: 3),
                      child: Center(
                        child: Text(
                          'После направления садового поста налево',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.5,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//Draille+des+Morts,+Saint-Martin-de-Crau/@43.6308653,4.7708582,15z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60b9781a2d0e3:0xdec61982f21d80a4!2m2!1d4.779613!2d43.6308656!3e0?hl=fr';
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.lightBlueAccent[200],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 135,
                            width: 200,
                            margin: const EdgeInsets.only(top: 50, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТ C\nSAINT MARTIN DE CRAU / ОФИСНАЯ ЭКСПЕДИЦИЯ\nДВЕРЬ 34", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),
                      )

                  ),
                  Container(
                    child: Image.asset('images/bat c.png'),
                    color: Colors.lightBlueAccent[200],
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                  ),
                  Container(
                      color: Colors.lightBlueAccent[200],
                      height: 100,
                      width: 55,
                      margin: const EdgeInsets.only(top: 50, left: 3),
                      child: Center(
                        child: Text(
                          'После направления садового поста направо',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.5,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                        onTap:() async {
                          const url = 'https://www.google.fr/maps/dir//1+Avenue+Marie+Curie,+13310+Saint-Martin-de-Crau/@43.6185727,4.7894338,16z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60bf40c8e43d5:0x8f285cd6c1febb5a!2m2!1d4.7938112!2d43.6185728!3e0?hl=fr';
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          } else {
                            throw 'Could not launch $url';
                          }
                        },
                        child: Container(

                            decoration: BoxDecoration(
                              color: Colors.lightGreen,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 135,
                            width: 200,
                            margin: const EdgeInsets.only(top: 50, left: 2),
                            child: Center(
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black, fontSize: 18),
                                  children: <TextSpan>[
                                    TextSpan(text: "БАТИМЕНТ DYNA\nSAINT MARTIN DE CRAU / ОФИСНАЯ ЭКСПЕДИЦИЯ\nДВЕРЬ 72", style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\nНажмите здесь, чтобы перейти к картам', style: TextStyle(color: Colors.black, fontSize: 18,)),
                                  ],
                                ),

                                textAlign: TextAlign.center,
                              ),
                            )

                        ),
                      )

                  ),
                  Container(
                    child:
                    Image.asset('images/dyna.png'),
                    color: Colors.lightGreen,
                    height: 100,
                    width: 90,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                  ),
                  Container(
                    color: Colors.lightGreen,
                    height: 100,
                    width: 55,
                    margin: const EdgeInsets.only(top: 50, left: 3),
                    child: Center(
                      child: Text(
                        'Месторасположение здания',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 20,
                    width: 356,
                    margin: const EdgeInsets.only(top: 10, right: 1, left: 2),
                  )

                ],
              )

            ],
          ),
        )

      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

