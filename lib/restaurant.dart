
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class Restaurant extends StatefulWidget {
  Restaurant({Key key}) : super(key: key);

  @override
  _RestaurantState createState() => _RestaurantState();
}

class _RestaurantState extends State<Restaurant> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: new Text('RESTAURANTS')),

      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap:() async {
                      const url = 'https://www.google.fr/maps/dir//Le+Resto+de+la+Gare,+D24,+13310+Saint-Martin-de-Crau/@43.6225644,4.7985888,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60bfaa0b8459f:0xd58a60fedc46c7b7!2m2!1d4.8007775!2d43.6225605!3e0';

                      if (await canLaunch(url)) {
                        await launch(url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.indigo[600],
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 200,
                        width: 340,
                        margin: const EdgeInsets.only(top: 20, left: 4, right: 4),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black, fontSize: 18),
                              children: <TextSpan>[

                                TextSpan(text: 'LE RESTO DE LA GARE \n', style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)),
                                TextSpan(text: 'chemin de la Gare, D24, 13310 Saint Martin de Crau\n ', style: TextStyle(color: Colors.white, fontSize: 20,)),
                                TextSpan(text: '04 90 47 05 18\n ', style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)),
                                TextSpan(text: '\nClick here to maps', style: TextStyle(color: Colors.white, fontSize: 20,)),
                              ],
                            ),

                            textAlign: TextAlign.center,
                          ),
                        )
                    ),
                  )

                  )

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap:() async {
                      const url = 'https://www.google.fr/maps/dir//8+Avenue+Marcellin+Berthelot,+13310+Saint-Martin-de-Crau/@43.6286538,4.7659829,13z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60be60fcee875:0xe662064820aa805f!2m2!1d4.8010023!2d43.628597!3e0';

                      if (await canLaunch(url)) {
                        await launch(url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.brown[600],
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 200,
                        width: 345,
                        margin: const EdgeInsets.only(top: 20, left: 4, right: 4),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black, fontSize: 18),
                              children: <TextSpan>[

                                TextSpan(text: 'COURTEPAILLE \n', style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)),
                                TextSpan(text: ' 8, Avenue Marcellin Berthelot 13310 Saint Martin de Crau\n ', style: TextStyle(color: Colors.white, fontSize: 20,)),
                                TextSpan(text: '04 90 18 25 35\n ', style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)),
                                TextSpan(text: '\nClick here to maps', style: TextStyle(color: Colors.white, fontSize: 20,)),
                              ],
                            ),
                            textAlign: TextAlign.center,
                          ),
                        )
                    ),)
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap:() async {
                      const url = 'https://www.google.fr/maps/dir//Lieu-dit+La+Samatane,+13310+Saint-Martin-de-Crau/@43.635609,4.8348905,12z/data=!4m2!4m1!3e0';

                      if (await canLaunch(url)) {
                        await launch(url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.red[900],
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 200,
                        width: 345,
                        margin: const EdgeInsets.only(top: 20, left: 4, right: 4),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black, fontSize: 20),
                              children: <TextSpan>[

                                TextSpan(text: 'LA CABANE BAMBOU \n', style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)),
                                TextSpan(text: 'Lieu-dit La Samatane 13310 Saint Martin de Crau\n', style: TextStyle(color: Colors.white, fontSize: 18,)),
                                TextSpan(text: '04 90 50 62 52\n', style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)),
                                TextSpan(text: '\nClick here to maps', style: TextStyle(color: Colors.white, fontSize: 20,)),
                              ],
                            ),

                            textAlign: TextAlign.center,
                          ),
                        )
                    ),
                      )
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap:() async {
                      const url = 'https://www.google.fr/maps/dir//CAMPING+SAINT+GABRIEL+***,+Quartier+Saint+Gabriel,+Route+de+Fontvieille,+13150+Tarascon/@43.7669111,4.6577062,13z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b5dc64ae4744dd:0x9ec3b2fdb192ccf0!2m2!1d4.6927256!2d43.7668545!3e0';

                      if (await canLaunch(url)) {
                        await launch(url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.green[600],
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 200,
                        width: 345,
                        margin: const EdgeInsets.only(top: 20, left: 4, right: 4),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black, fontSize: 18),
                              children: <TextSpan>[

                                TextSpan(text: 'LA CLÉ DES CHAMPS \n', style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)),
                                TextSpan(text: "Quartier Sainte-Gabrielle Route d'Arles, 13150 Tarascon\n", style: TextStyle(color: Colors.white, fontSize: 20,)),
                                TextSpan(text: '04 90 49 76 19\n', style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)),
                                TextSpan(text: '\nClick here to maps', style: TextStyle(color: Colors.white, fontSize: 20,)),
                              ],
                            ),

                            textAlign: TextAlign.center,
                          ),
                        )
                    ),)
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap:() async {
                      const url = 'https://www.google.fr/maps/dir//H%C3%B4tel+Ibis+Budget+Porte+de+Camargue,+Zone+Ecopole,+Avenue+Marcellin+Berthelot,+13310+Saint-Martin-de-Crau/@43.6286956,4.7687093,13z/data=!4m9!4m8!1m0!1m5!1m1!1s0x12b60be67e12ac27:0x4fd7e4bd16edfc67!2m2!1d4.8037287!2d43.6286388!3e0';

                      if (await canLaunch(url)) {
                        await launch(url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.blue[900],
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 200,
                        width: 345,
                        margin: const EdgeInsets.only(top: 20, left: 4, right: 4, bottom: 20),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black, fontSize: 18),
                              children: <TextSpan>[

                                TextSpan(text: "McDonald's\n", style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)),
                                TextSpan(text: "Avenue Marcellin Berthelot Zone,Écopôle 13310 Saint Martin de Crau\n", style: TextStyle(color: Colors.white, fontSize: 18,)),
                                TextSpan(text: '04 90 52 38 82\n', style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)),
                                TextSpan(text: '\nClick here to maps', style: TextStyle(color: Colors.white, fontSize: 20,)),
                              ],
                            ),

                            textAlign: TextAlign.center,
                          ),
                        )
                    ),
                      )
                  ),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}
